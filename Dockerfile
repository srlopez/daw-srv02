FROM node
WORKDIR /app
COPY package.json .
RUN npm install
COPY server.js .
#CMD node server.js
ENV hello=Docker
ENTRYPOINT [ "node", "server.js" ]
EXPOSE 5000