const express = require('express')
const app = express()

const port = process.argv[2] || 5000
const hello = process.env.hello || "World"
const id = (new Date()).getTime().toString(36)

const server = app.listen(port, () => {
    console.log(`Server Started on Port  ${port}`)
})

app.get('/', (req, res) => {
     var port = server.address().port;
     var hostname = require('os').hostname();
     var ip = require('os').networkInterfaces().eth0? 
                require('os').networkInterfaces().eth0[0].address:
                '0.0.0.0'
     var interfaces = (ip!='0.0.0.0')?'':require('os').networkInterfaces()

     res.set('Content-Type', 'text/plain')
        .status(200)
        //.send('` Hello ${hello} !`')
        .send({
            id: id,
            client: req.hostname+' '+req.ip,
            service: ip+':'+port,
            hostname: hostname,
            interfaces: interfaces
        })
})


